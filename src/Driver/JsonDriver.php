<?php

namespace Selene\ConfigProvider\Driver;

use Silex\Application;
use Selene\ConfigProvider\Driver\DriverInterface;
use Selene\ConfigProvider\Exception\FileNotFound;

class JsonDriver implements DriverInterface
{
    /**
     * The data from the JSON file as an associative array
     */
    protected $json;

    public function loadFile($filename)
    {
        if (!file_exists($filename)) {
            throw new FileNotFound($filename);
        }
        $this->json = json_decode(file_get_contents($filename), true);
        return (bool)$this->json;
    }

    public function process(Application $app)
    {
        foreach($this->json as $name => $value) {
            $app[$name] = $value;
        }
    }
}
