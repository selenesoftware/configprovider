<?php

namespace Selene\ConfigProvider\Driver;

use Silex\Application;
use Symfony\Component\Yaml\Yaml;
use Selene\ConfigProvider\Driver\DriverInterface;
use Selene\ConfigProvider\Exception\FileNotFound;

class YamlDriver implements DriverInterface
{

    protected $yaml;

    public function loadFile($filename)
    {
        if(!file_exists($filename)) {
            throw new FileNotFound($filename);
        }

        $this->yaml = Yaml::parse(file_get_contents($filename));
        return (bool)$this->yaml;
    }

    public function process(Application $app)
    {
        foreach($this->yaml as $name => $value) {
            $app[$name] = $value;
        }
    }
}

