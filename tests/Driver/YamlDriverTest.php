<?php

namespace Selene\ConfigProvider\Driver;

use Silex\Application;
use Selene\ConfigProvider\Driver\YamlDriver;
use Selene\ConfigProvider\Exception\FileNotFound;
use PHPUnit\Framework\TestCase;

/**
 * Why does this look like the JsonDriver tests?  Becasue I just copied and changed Json with Yaml
 */
class YamlDriverTest extends TestCase
{
    public function testLoadFile()
    {
        $driver = new YamlDriver;
        $this->assertTrue($driver->loadFile('test.yaml'));
    }

    public function testLoadFileNotFound()
    {
        $this->expectException(FileNotFound::class);
        $driver = new YamlDriver;
        $driver->loadFile('not_found.yaml');
    }

    public function testProcessFile()
    {
        // Oh noes!  Why aren't I actually mocking the application?  Because of offsetSet, you dumb turds.
        // It's hard to create the mock with offsetSet and have it do what it is supposed to do.
        // So in the interest of time and sanity, I'm calling the application and using it.
        $mock = new Application;

        $driver = new YamlDriver;
        $driver->loadFile('test.yaml');
        $driver->process($mock);
        $this->assertTrue($mock['debug']);
    }

}
